let express = require('express');
bodyParser = require('body-parser');
let app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
var routes = require('./api/routes/route'); //importing route
routes(app); //register the route
let port = process.env.PORT || 3000;

app.listen(port);

console.log('RESTful API server started on: ' + port);