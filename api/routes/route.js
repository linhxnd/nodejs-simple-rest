'use strict';
module.exports = function(app) {
  var todoList = require('../controllers/MainController');

  // todoList Routes
  app.route('/login')
    .get(todoList.view_login);
};
